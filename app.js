/*
 * This is a very simple and naive http server that serves as a test platform
 * for the client side (dash cam) to verify the behavior of the server. When
 * the client sends the right data, the server should respond with the right
 * behavior.
 *
 * Please feel free to modify it if necessary.
 */

var http = require("http");
var url = require("url");
var fs = require("fs");
var PORT = 5555;

// TODO: Implement this.
function validateJSON(json) {
	return true;
}

// As its name suggests.
function errorHandler(error, res) {
	console.error(error);
	res.writeHead(404, {"Content-Type": "application/json"});
	res.write(JSON.stringify({"result": "failed", "reason": error.toString()}));
	return res.end();
}

// As its name suggests.
function handleDashCamJSON(req, res) {
	var mockUploadPath = "http://localhost:5555/";
	var fileId = "23441";
	var imgUploadPath = mockUploadPath + "image/" + fileId;
	var videoUploadPath = mockUploadPath + "video/" + fileId;
	var data = "";

	req.on("data", function(chunk) {
		data += chunk;
	}).on("end", function() {
		if (data !== "") {
			console.log("Received JSON:");
			console.log(data);
			try {	
				data = JSON.parse(data);
				if (!validateJSON(data)) return errorHandler("Incorrect format", res);
				var ret = {"result": "ok"};
				if (data["has_image"]) ret["image_path"] = imgUploadPath;
				if (data["has_video"]) ret["video_path"] = videoUploadPath;
				res.writeHead(200, {"Content-Type": "application/json"});
				res.write(JSON.stringify(ret));
				return res.end();
			} catch(e) {
				return errorHandler(e, res);
			}
		} else return errorHandler("Incorrect format", res);
	});
};

// As its name suggests.
var handleFileUpload = function(type, fileId, req, res) {
	var attr = (type === "image")? ".jpg": ".mp4";
	var fileName = fileId+attr;
	var writeStream = fs.createWriteStream("./"+fileName);

	req.on("data", function(data) {
		writeStream.write(data);
	}).on("end", function() {
		writeStream.end();
		console.log("File " + fileName + " saved");
	});

	var ret = {"result": "ok"};
	res.writeHead(200, {"Content-Type": "application/json"});
	res.write(JSON.stringify(ret));
	return res.end();
}

// Start server
http.createServer(function(req, res) {
	var path = url.parse(req.url).pathname;

	if (req.method === 'POST' && path === "/dashcam") {
		return handleDashCamJSON(req, res);
	} else if (req.method === 'PUT') {
		var type = path.split("/")[1];
		var fileId = path.split("/")[2];
		if (type === "video" || type === "image") return handleFileUpload(type, fileId, req, res);
	}

	res.writeHead(400, {"Content-Type": "application/json"});
	res.write(JSON.strigify({"result": "fail", "reason": "Unknown operation"}));
	res.end();
}).listen(PORT, function() {
  console.log('Listening for requests on port ' + PORT);
});

